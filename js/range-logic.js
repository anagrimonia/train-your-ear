// ---------------------- Piano Range functionality ---------------------- //

// Supporing array of note letters
letters = {'C' : 0, 'D' : 1, 'E' : 2, 'F' : 3, 'G' : 4, 'A' : 5, 'B' : 6};

// Compare two notes
function compareNotes(l, r) {
    let a = letters[l.charAt(0)] + parseInt(l.charAt(1)) * 10 + (l.charAt(2) == '#' ? 0.5 : 0);
    let b = letters[r.charAt(0)] + parseInt(r.charAt(1)) * 10 + (r.charAt(2) == '#' ? 0.5 : 0);

    return a - b;
}

// Piano range selection 
function selectRange() {

    $('.range').each(function(e) {

        let l = compareNotes($(this).attr('id'), prms.range[0]);
        let r = compareNotes($(this).attr('id'), prms.range[1]);

        if (l >= 0 && r <= 0)
        {
            if ($(this).hasClass('piano-key-white'))
                $(this).css("background-color", "rgba(255, 255, 255, 0.8)");
            else
                $(this).css("background-color", "#202020");
        }
        else
            $(this).css("background-color", "");
        
    });
}

// ---------------------- Find Range Mode functionality ---------------------- //

// Click event on "Check vocal range" button
$(document).on('click', '#btn-check-range', function(e) {

    $('#piano-wrapper').css("background-color", "rgb(0,0,0,0)");

    let info = $('<label/>', { id: 'info', 
                                class: 'centering-x hidden',
                                text: "Please, allow to use your microphone.." });
    $('#content').append(info);

    $('#content').children().not('#piano-wrapper').animate({opacity: 0}, 250, function(e) {
        $('#content').children().not('#piano-wrapper').not('.cross').hide();
    });

    $('#piano-wrapper').animate({top: '20'}, 800, function(e) { 
        $('#info').show().animate({opacity: 1}, 500)
    });

    $('#area').append('<img id="back" src="img/back.png"></img>');
    $('#back').hide().fadeIn();

    $('.range').css("pointer-events", "none");

    getAudioRangeMode();
});

// Click event on the back icon
$(document).on('click', '#area #back', function(e) {

    stopAudio();

    $('#back').fadeOut();

    $('#info').animate({opacity: 0}, 300, function(e) {
        $('#content').children().not('#piano-wrapper').show();
        $('#info').remove();
    })

    $('#piano-wrapper').animate({top: '240'}, 800, function(e) { 
        $('#content').children().not('#piano-wrapper').animate({opacity: 1}, 250, function(e) {
        });
    });
});

// Change animation function
function changeTextAnimation(obj, text) {
    $(obj).animate({top: '-=' + 20, opacity: 0}, 250, function(e) {
        $(obj).html(text);
        $(obj).css('top', $(obj).position().top + 40);
        $(obj).animate({top: '300px', opacity: 1}, 250);
    });
}

// Click event on "try again to detect microphone" button
$(document).on('click', '#info-try-again', function(e) {
    if ($('#btn-start').is(":hidden")) getAudioRangeMode();
    else getAudioGamePlay();
    changeTextAnimation('#info', 'Please, allow to use your microphone..');
});

// Helpful array for note clarity detection
var arrayDetection = [];

// Get audio in range mode
function getAudioRangeMode() {
   navigator.mediaDevices.getUserMedia({audio: true, video: false}) 
        .then(function(stream) {
            getAudio(stream);

            if (!$('#btn-start').is(":hidden")) {
                stopAudio(); 
                return;
            }; 

            prms.range = [];
            flagsRangeMode = [true, true];

            $('.range').each(function(e) { 
                $(this).css("background-color", "") });

            detectRangeMode();
    }).catch(function(error) { errorRangeMode('info') });
}

// Supporting flags for range mode
var flagsRangeMode = [true, true];

// Functionality for range mode
async function detectRangeMode() {
    if (flagsRangeMode[0]) {
            console.log("h");

        arrayDetection = [];
        changeTextAnimation('#info', 'Find the <label style="font-size: 25px; font-weight: bold">lowest</label> note you can sing in your normal voice. \
                                            <br>Try to sing it clearly... \
                                            <img id="sound-detector" class="centering-x" src="img/micro.png"></img> \
                                            <div id="progress-bar" class="centering-x"><div id="progress-bar-filler"></div></div>');
        flagsRangeMode[0] = false;
    }

    if (!prms.range[0]) {
        
        prms.range[0] = findNoteRangeMode();
        return;
    }

    if (flagsRangeMode[1]) {
        arrayDetection = [];
        changeTextAnimation('#info', 'Great! Your lowest note: ' + prms.range[0]);
        await sleep(2000);
        changeTextAnimation('#info', 'Find the <label style="font-size: 25px; font-weight: bold">highest</label> note you can sing in your normal voice. \
                                            <br>Try to sing it clearly... \
                                            <img id="sound-detector" class="centering-x" src="img/micro.png"></img> \
                                            <div id="progress-bar" class="centering-x"><div id="progress-bar-filler"></div></div>');
        flagsRangeMode[1] = false;
    }

    if (!prms.range[1]) {
        prms.range[1] = findNoteRangeMode();
        return;
    }

    changeTextAnimation('#info', 'Splendid! Your highest note: ' + prms.range[1]);
    await sleep(2000);
    selectPianoKey('[id="' + prms.range[0] + '"]');
    selectPianoKey('[id="' + prms.range[1] + '"]');
    $('.range').css("pointer-events", "");
    $('#area #back').trigger("click");
}

// In case of a microphone detection error
function errorRangeMode(id) {
    changeTextAnimation('#' + id +'', 'Microphone is not detected. \
                        <div id="' + id + '-try-again">Click here to try again</div>');
}

// Find note function for range mode
function findNoteRangeMode(stop) {

    // getting note string
    let str = getNoteFromAudio();
    let note;

    if (str != -1 && str.length < 4) {
        $('#sound-detector').css('opacity' , '1');                                
        // example: ['C2' : {note: Note('C2'), count: 1}]

        curr = new Note(str);
        ccurr = (arrayDetection[curr.str]) ? arrayDetection[curr.str].count : 0;
                  
        prev = curr.getPrevNote();
        cprev = (arrayDetection[prev.str]) ? arrayDetection[prev.str].count : 0;

        next = curr.getNextNote();
        cnext = (arrayDetection[next.str]) ? arrayDetection[next.str].count : 0;

        // arrayDetection = []

        arrayDetection[curr.str] = { note: curr, count: ccurr + 1 };
        arrayDetection[prev.str] = { note: prev, count: cprev + 0.5 };
        arrayDetection[next.str] = { note: next, count: cnext + 0.5 };

        if (   arrayDetection[curr.str].count >= 100
            && arrayDetection[curr.str].count <= 102) {            
            detectRangeMode();
            return arrayDetection[next.str].note.str;
        }

        console.log(arrayDetection);
        $('#progress-bar-filler').css('width', arrayDetection[curr.str].count + '%');
    }
    else $('#sound-detector').css('opacity' , '0.25'); 

    setTimeout(requestAnimationFrame(function() {detectRangeMode()}), 50);
}

// Click event on start game button
$(document).on('click', '#btn-start', function(e) {
    gameSelStart(this)
});