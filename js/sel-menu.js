
// ---------------------- Selection menu functionality ---------------------- //

// {game: guess-note|guess-interval|build-interval, way: piano|voice, range = [l, r],
//  count: 5..100, timer: 5...500 }
var prms = {};

// Expand start button
$("#button-start").click(function(e) {
    $("#button-settings #cross").trigger("click");
    $("#button-about #cross").trigger("click");
    showArea();

});

// Create area
function showArea() {
    $("#overlay").animate({opacity: 0.65}, 1000);
    $("#main-menu").animate({top: "0", opacity: 0}, 600,
        function(e) {
            $("#area").animate({opacity: 1}, 500,
            showGameSelection());
    });

    $('#area').append('<img id="exit" class="cross" src="img/cross.png"></img>');
}

// Click event in exit button
$(document).on('click', '#exit', function(e) { 

    delete prms, input, output, count;
    stopAudio();

    $("#area").animate({opacity: 0}, 600,
        function(e) {
            $("#overlay").animate({opacity: 0.0}, 1000);
            $("#main-menu").animate({top: "50%", opacity: 1}, 600);
            $('#content').empty();
            $('#exit').remove();
        });
});

// Show game selection menu
function showGameSelection() {

    $("#content").append('<label class="title">SELECT THE GAME:</label>');

    let gameselmenu = $('<div/>', {
                id: 'game-sel-menu',
                class: 'sel-menu', 
    }); 

    for (var i = 0; i < 3; ++i) {
        let button = $('<label/>', {
                    id: 'game-sel-menu-item-' + (i + 1),
                    class: 'game-sel-menu-item', 
                    click: function() { gameSelMenuSelected(this) }
        }); 

        $(gameselmenu).append(button);
    } 

    $("#content").append(gameselmenu);

    $('#game-sel-menu-item-1').html('Guess the note');
    $('#game-sel-menu-item-2').html('Guess the interval');
    $('#game-sel-menu-item-3').html('Build the interval');

    $("#content").append('<label class="title">SELECT THE WAY TO ANSWER:</label>');

    let toolselmenu = $('<div/>', {
                id: 'tool-sel-menu',
                class: 'sel-menu', 
    }); 

    for (var i = 0; i < 2; ++i) {
        let button = $('<label/>', {
                    id: 'tool-sel-menu-item-' + (i + 1),
                    class: 'tool-sel-menu-item', 
                    click: function() { toolSelMenuSelected(this) }
        }); 

        $(toolselmenu).append(button);
    } 

    $("#content").append(toolselmenu);

    $('#tool-sel-menu-item-1').html('Piano');
    $('#tool-sel-menu-item-2').html('Voice');

    $("#content").append('<label class="title">CHECK MUSICAL RANGE:</label>');

    createRangeButton();
    wrapper = createPiano('range');
    wrapper.css("top", "250px");
    $('#content').append(wrapper);
    $('#piano').scrollLeft(($('#piano-white').width() - $('#piano').width()) / 2);

    createAdditionalParams(); 

    createStartButton();
}

// Create button for vocal range checking
function createRangeButton() {
    let button = $('<label/>', { id: 'btn-check-range', 
                                class: 'centering-x',
                                text: "What\'s my vocal range?" });
    $('#content').append(button);
}

// Create additional selection parameters
function createAdditionalParams() {
    let addselmenu = $('<div/>', {
                id: 'add-sel-menu',
                class: 'sel-menu centering-x', 
    }); 

    let count = $('<div/>', { id: 'count-sel-menu', class: 'add-sel-menu-item' }); 
    let timer = $('<div/>', { id: 'timer-sel-menu', class: 'add-sel-menu-item' }); 

    count.append('<label>Count (5..100): </label><input type="text" id="count-sel-menu-input" value="10">');
    timer.append('<label>Timer (15..500): </label><input type="text" id="timer-sel-menu-input" value="20">');

    $(addselmenu).append(count);
    $(addselmenu).append(timer);

    $("#content").append(addselmenu);
}

// Click event on count parameter
$(document).on('click', '#count-sel-menu-input', function(e) { 
    $('#count-sel-menu-input').css('background-color', '');
});

// Click event on timer parameter
$(document).on('click', '#timer-sel-menu-input', function(e) { 
    $('#timer-sel-menu-input').css('background-color', '');
});

// Create game start button
function createStartButton() {

    let start = $('<div/>', { id: 'btn-start',
                              class: 'title centering-x btn-start-hover', 
                              click: function() {
                              //alert($(this).attr('data-system-id'));
                            }
    }); 

    start.html('<label>START THE GAME</label>');

    $('#content').append(start);    
}

// ---------------------- Selection buttons behavior ---------------------- //

// Animation of game selection
function gameSelMenuSelected(obj) {
        if ($(obj).hasClass('menu-item-selected'))
        {
            $(obj).removeClass('menu-item-selected').removeAttr("style");
            delete prms.game;
        }
        else {
            $('.game-sel-menu-item').removeAttr("style");
            $('.game-sel-menu-item').not(obj).removeClass('menu-item-selected');
            $(obj).addClass('menu-item-selected');
            prms.game = $(obj).text();

            if (prms.game == "Guess the interval") {
                $('.tool-sel-menu-item').removeClass('menu-item-selected');
                $('.tool-sel-menu-item').css("background-color", "");
                $('.tool-sel-menu-item').animate({opacity: "0.25"}, 250);
                $('.tool-sel-menu-item').css("pointer-events","none");
                prms.way = "None";
            }
            else if (prms.game == "Build the interval") {
                $('#tool-sel-menu-item-2').removeClass('menu-item-selected');
                $('#tool-sel-menu-item-2').animate({opacity: "0.25"}, 250);
                $('#tool-sel-menu-item-2').css("pointer-events","none");
                $('#tool-sel-menu-item-1').css("pointer-events", "auto");
                $('#tool-sel-menu-item-1').animate({opacity: "1"}, 250);
                delete prms.way;
            }
            else {
                $('.tool-sel-menu-item').css("pointer-events", "auto");
                $('.tool-sel-menu-item').animate({opacity: "1"}, 250);
                if (prms.way == "None") 
                    delete prms.way;

            }
        }
    }

// Animation of tool selection
function toolSelMenuSelected(obj) {
        if ($(obj).hasClass('menu-item-selected'))
        {
            $(obj).removeClass('menu-item-selected').removeAttr("style");
            delete prms.way;
        }
        else {
            $('.tool-sel-menu-item').css('background-color' , '');
            $('.tool-sel-menu-item').not(obj).removeClass('menu-item-selected');
            $(obj).addClass('menu-item-selected');
            prms.way = $(obj).text();
        }
    }

// Check selected parameters before the game process
function gameSelStart(obj) {

    let timer = parseInt($('#timer-sel-menu-input').val());
    let count = parseInt($('#count-sel-menu-input').val());

    if (prms.range.length != 2 || prms.range[0] == undefined || prms.range[1] == undefined ||
        prms.game == undefined || prms.way == undefined || 
        isNaN(count) || count < 5 || count > 100 || isNaN(timer) || timer < 15 || timer > 500) {

        if (prms.range.length != 2 || prms.range[0] == undefined || prms.range[1] == undefined)
            $("#piano-wrapper").css('background-color', 'rgb(255, 150, 160, 0.3)');

        if (prms.game == undefined)
            $(".game-sel-menu-item").css('background-color', 'rgb(255, 150, 160, 0.3)');

        if (prms.way == undefined)
            $(".tool-sel-menu-item").css('background-color', 'rgb(255, 150, 160, 0.3)'); 

        if (isNaN(count) || count < 5 || count > 100)       
            $('#count-sel-menu-input').css('background-color', 'rgb(255, 150, 160, 0.3)');

        if (isNaN(timer) || timer < 15 || timer > 500)       
            $('#timer-sel-menu-input').css('background-color', 'rgb(255, 150, 160, 0.3)');

        return;
    }

    prms.timer = timer;
    prms.count = count;

    let info = $('<label/>', { id: 'info', 
                            class: 'centering-x hidden',
                            text: "Please, allow to use your microphone.." });
    $('#content').append(info);

    flag = true;
    $('#content').children().animate({opacity: 0}, 250, function(e) {
        if (flag) {
            $('#content').children().remove();
            buildGameArea();
            flag = false;
        }
    });
}