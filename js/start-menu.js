// ---------------------- Start menu animation functionality ---------------------- //

var flag_settings, flag_about, game_vol = 1;

// Click event on volume change
$(document).on('change', '#volume', function() {
    game_vol = $('#volume').val() / 100;
    playPianoNote('A4');
    console.log("sd");
});

// Expand menu button
function expandButton(obj) {
    $(obj).empty();
    $(obj).animate({height: "+=" + 400}, 500,
        function () { 
            $(obj).append('<img id="cross" src="img/cross.png"></img>');

         });
}

// Reduce menu button
function reduceButton(obj, name) {
    $(obj).animate({height: "-=" + 400}, 500);
    $(obj).children().fadeOut(function(e) { $(obj).empty(); 
    $(obj).append('<label class="centering">' + name + '</label>');});
}

// Expand settings button
$("#button-settings").click(function(e) {
    if (flag_settings) return;
    flag_settings = true;
    $("#button-about #cross").trigger("click");
    expandButton(this);

    vol = $('<div/>', { id: 'volume-wrapper', class: 'centering' });
    $(vol).append("<label>Volume: </label>");
    $(vol).append('<input type="range" id="volume" min="0" max="100" orient="vertical">');
    $('#button-settings').append(vol);
    $('#volume-wrapper').hide().fadeIn();

    return false;
});

// Expand about button
$("#button-about").click(function(e) {
    if (flag_about) return;
    flag_about = true;
    $("#button-settings #cross").trigger("click");
    expandButton(this);
    about = $('<div/>', { id: 'about', class: 'centering' });
    about.append('<label>The program is designed to facilitate \
                            the process of mastering solfeggio and to improve\
                             vocal skills. <br><br>The proposed game modes can help the \
                             user to develop the ability to recognize notes, build \
                             musical intervals.<br><br>Developer: Anastasiia Kolonskaia</label>' );
    $('#button-about').append(about);
    $('#about').hide().fadeIn();
});

// Reduce settings button
$(document).on('click', "#button-settings #cross", function(e) {
    if (!flag_settings) return;
    flag_settings = false;
    reduceButton($(this).parent(), 'Settings');
    
});

// Reduce about button
$(document).on('click', "#button-about #cross", function(e) {
    if (!flag_about) return;
    flag_about = false;
    reduceButton($(this).parent(), 'About');
});