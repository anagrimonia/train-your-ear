var audio, volume, array_frequency, frequency, audioContext, analyser, 
    microphone, amplitude, audioStream;
  
// Get audio function
function getAudio(stream) {
	audioStream = stream;
	audioContext = new AudioContext();
    analyser = audioContext.createAnalyser();
    analyser.fftSize = 2048;
    array_frequency = new Float32Array(analyser.frequencyBinCount);
    amplitude = new Uint8Array(analyser.frequencyBinCount);
    volume = audioContext.createGain();
    
    microphone = audioContext.createMediaStreamSource(stream);
    microphone.connect(volume);
    microphone.connect(analyser);
} 

// Stop audio function
function stopAudio() {
	if (audioStream) {
		var track = audioStream.getTracks()[0];  // if only one media track
		track.stop();
		audioStream = undefined;
		console.log("g");
	}
}

function getNoteFromAudio () {
      analyser.getFloatTimeDomainData(array_frequency);
      frequency = autoCorrelate(array_frequency, audioContext.sampleRate);
      return calculateNote(frequency);
    };

function getFrequencyFromAudio () {
      analyser.getFloatTimeDomainData(array_frequency);
      frequency = autoCorrelate(array_frequency, audioContext.sampleRate);
      return frequency;
    };

var MIN_SAMPLES = 0;  // will be initialized when AudioContext is created.
var GOOD_ENOUGH_CORRELATION = 0.9; // this is the "bar" for how close a correlation needs to be

var buflen = 1024;
var buf = new Float32Array( buflen );

function autoCorrelate(buf, sampleRate) {
	var SIZE = buf.length;
	var MAX_SAMPLES = Math.floor(SIZE/2);
	var best_offset = -1;
	var best_correlation = 0;
	var rms = 0;
	var foundGoodCorrelation = false;
	var correlations = new Array(MAX_SAMPLES);

	for (var i=0;i<SIZE;i++) {
		var val = buf[i];
		rms += val*val;
	}
	rms = Math.sqrt(rms/SIZE);

	if (rms < 0.008) // not enough signal
		return -1;

	var lastCorrelation=1;
	for (var offset = MIN_SAMPLES; offset < MAX_SAMPLES; offset++) {
		var correlation = 0;

		for (var i = 0; i < MAX_SAMPLES; i++) {
			correlation += Math.abs((buf[i])-(buf[i+offset]));
		}
		correlation = 1 - (correlation/MAX_SAMPLES);
		correlations[offset] = correlation; // store it, for the tweaking we need to do below.
		if ((correlation > GOOD_ENOUGH_CORRELATION) && (correlation > lastCorrelation)) {
			foundGoodCorrelation = true;
			if (correlation > best_correlation) {
				best_correlation = correlation;
				best_offset = offset;
			}
		} else if (foundGoodCorrelation) {
			// short-circuit - we found a good correlation, then a bad one, so we'd just be seeing copies from here.
			// Now we need to tweak the offset - by interpolating between the values to the left and right of the
			// best offset, and shifting it a bit.  This is complex, and HACKY in this code (happy to take PRs!) -
			// we need to do a curve fit on correlations[] around best_offset in order to better determine precise
			// (anti-aliased) offset.

			// we know best_offset >=1, 
			// since foundGoodCorrelation cannot go to true until the second pass (offset=1), and 
			// we can't drop into this clause until the following pass (else if).
			var shift = (correlations[best_offset+1] - correlations[best_offset-1])/correlations[best_offset];  
			return sampleRate/(best_offset+(8*shift));
		}
		lastCorrelation = correlation;
	}
	if (best_correlation > 0.01) {
		//console.log("f = " + sampleRate/best_offset + "Hz (rms: " + rms + " confidence: " + best_correlation + ")")
		return sampleRate / best_offset;
	}

	// console.log("f = " + sampleRate/best_offset + "Hz (rms: " + rms + " confidence: " + best_correlation + ")")
	return -1;
    // var best_frequency = sampleRate/best_offset;
}

var notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"];

// return string
function craftNote(octave, letter) {
	return (letter.length == 1) ? letter + octave : letter[0] + octave + letter[1];
}

function calculateNote(frequency) {
	if (frequency == -1)
		return -1;

	c0 = 440 *  Math.pow(2, -57/12);  // 'C0 is 57 semitones below A4 (440 Hz)
	nsemi = 12 * Math.log(frequency / c0) / Math.log(2);
	octave = parseInt(nsemi / 12);
	letter = notes[parseInt(nsemi - 12 * octave)];
	return craftNote(octave, letter);
}

function playNote(frequency) {
	
	if (!audioContext) 
		audioContext = new AudioContext();

    osc = audioContext.createOscillator();
    osc.frequency.value = frequency;
    osc.type = "sine";
    osc.connect(audioContext.destination);

    var gain = audioContext.createGain();
    osc.connect(gain);
    gain.connect(audioContext.destination);

    var now = audioContext.currentTime;

	gain.gain.setValueAtTime(0, now);
	gain.gain.linearRampToValueAtTime(0.5, now + 0.1);

	osc.start();

	gain.gain.linearRampToValueAtTime(0, now + 0.99);
	osc.stop(now + 1);
}

function playPianoNote(note) {
	aud = new Audio();
	if (note.length == 3) note = note.replace('#', 's');
	aud.src = location.pathname + '/../sounds/Piano.pp.' + note + '.mp3';
	aud.volume = game_vol;
	aud.play().catch(function() {
    	aud.play();
	});
	setTimeout(function(){
        aud.pause();
        aud.currentTime = 0;
    }, 2000);
}