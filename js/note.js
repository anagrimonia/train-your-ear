// Note Class
function Note(string) {
    
    this.str = string;
    this.octave = string[1];
    this.letter = (string.length == 2) ? string[0] : string[0] + string[2];

    this.getPrevNote = function() {
        
        return (this.letter == 'C') ? new Note( craftNote(this.octave - 1, 'B') ) : 
                                      new Note( craftNote(this.octave, notes[notes.indexOf(this.letter) - 1]) );
    }

    this.getNextNote = function() {

        return (this.letter == 'B') ? new Note( craftNote(this.octave - 1, 'C') ) : 
                                      new Note( craftNote(this.octave, notes[notes.indexOf(this.letter) + 1]) );
    }
}