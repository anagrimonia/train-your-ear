
// Sleep function
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


var timer; // timer in game modes
var count = {}; // total, win, lose
var neigbours = []; // note neighbours for voice indicator
var borders = []; // note borders for voice indicator
var l, r; // borders of chosen range
var voicecount; // supporting count for musical note detection
var output; // output of the game round
var input; // input of the game round

// Build game area
function buildGameArea() {

    if (prms.way == "Piano") {

        question = createNotebox(); 
        answer = createPiano("piano-way");
        
    }
    else if (prms.way == "Voice") {

        question = createPiano("piano-game");
        answer = createVoiceInd();
    }
    else {
        question = createPiano("piano-game");
        answer = createIntervalBox();
    }

    question.addClass("top").hide().fadeIn('slow');
    answer.addClass("bottom").hide().fadeIn('slow');
    $('#content').append(question);
    $('#content').append(answer);


    let info = $('<label/>', { id: 'info', 
                               class: 'centering-x hidden' });
    $('#content').append(info);
    $("#info").animate({opacity: 1}, 250);

    let params = $('<div/>', {
                id: 'game-params',
                class: 'game-params centering-x', 
    }); 

    let timer_params = $('<div/>', { id: 'timer-game-params', class: 'game-params-item' }); 
    let count_params = $('<div/>', { id: 'count-game-params', class: 'game-params-item' }); 

    timer_params.append('<label id="game-left">Time left: </label> \
                        <img id="repeat" src="img/repeat.png">');
    count_params.append('<label id="game-win">Win: </label> \
                  <label id="game-lose">Lose: </label>\
                  <label id="game-total">Total: </label>');

    $(params).append(timer_params);
    $(params).append(count_params);

    $("#content").append(params);

    if (prms.way == 'Voice') 
        getAudioGamePlay();
    else {
        timer = prms.timer;
        count = {left: prms.count, win: 0, lose: 0};
        tickGame();
    }
}

// Get audio for game mode
function getAudioGamePlay() {

    changeTextAnimation('#info', "Please, allow to use your microphone..");

    navigator.mediaDevices.getUserMedia({audio: true, video: false}) 
        .then(function(stream) {
            getAudio(stream);

            if ($('#info').is(":hidden")) {
                stopAudio(); 
               return;
            }; 

            timer = prms.timer;
            count = {left: prms.count, win: 0, lose: 0};
            tickGame();

    }).catch(function(error) { errorRangeMode('info') })
}

// Tick function for game mode
async function tickGame() {

    if (output == undefined) {

        if (timer == prms.timer) {

            if (count.left == prms.count) changeTextAnimation('#info', 'Are you ready?');
            await sleep(2000);

            // Генерируем новый input
            getInput();
            
            if (prms.way == 'Voice') {
                changeTextAnimation('#info', "Note:\t" + '<label style="font-size: 25px; font-weight: bold">' + input + '</label>');
                voicecount = 0;
                findNoteGameMode(stop);
                $('#progress-bar-filler').css('width', 0);
            }
            else if (prms.game == "Guess the note" && prms.way == "Piano") {
                changeTextAnimation('#info', "Listen carefully...");
                $('.piano-way').css('background-color', '');
            }
            else if (prms.game == "Guess the interval") {
                changeTextAnimation('#info', "Listen carefully...");
                $('.interval').css('background-color', '');
            }
            else {
                $('.piano-way').css('background-color', '');
                changeTextAnimation('#info', "Build <label style='font-size: 25px; font-weight: bold'>" 
                    + interval + '</label> from the sounded note...');
            }
            
        }
        if (timer == 0) {

            changeTextAnimation('#info', "Oops, time is left!");

            timer = prms.timer;
            count.lose++;
            count.left--;
        }
        else 
            timer--; 

        $('#game-left').text('Time left: ' + timer);
    }
    else {

        timer = prms.timer;
        count.left--;

        if (input == output) {

            count.win++;
            changeTextAnimation('#info', "You've got it!");

        } 
        else { 
            count.lose++;
            changeTextAnimation('#info', "No, you are wrong :(");
        }

        if (prms.game == 'Guess the interval') {
            $('.interval').removeClass('interval-selected');
            $('#' + output + '-interval').css('background-color', 'rgb(220, 20, 60, 0.5)');
            $('#' + input + '-interval').css('background-color', 'rgb(34, 139, 34, 0.5)');
        }
        else if (prms.way == "Piano") {
            $('[id="' + output + '"]').css('background-color', 'rgb(220, 20, 60, 1)');
            $('[id="' + input + '"]').css('background-color', 'rgb(34, 139, 34, 1)');
        }

        output = undefined;
    }

    $('#game-win').text('Win: ' + count.win);    
    $('#game-lose').text("Lose: " + count.lose);
    $('#game-total').text('Total attempts: ' + (prms.count - count.left));

    if (count.left != 0)
        setTimeout(function() { tickGame(); }, 1000);
    else {
        $('#game-params').fadeOut();
        changeTextAnimation('#info', '<label style="font-size: 25px; font-weight: bold">' 
            + "Game over!" + '</label><br><br> \
            Wins: ' + count.win + '<br>Loses: ' + count.lose);
    }
}

// Supporting integers for game mode
var one, two, interval;

// Get input function
function getInput() {
 
    one = undefined;
    two = undefined;

    l = frequencies.findIndex(function(element, index, array)  {
        if (prms.range[0] == "A0") return 1;
        if (element.note == prms.range[0]) return index;
    });

    r = frequencies.findIndex(function(element, index, array)  {
        if (element.note == prms.range[1]) return index;
    });

    let li, ri;

    if (prms.game == 'Guess the note') {
        
        li = Math.floor(l + Math.random() * (r - l +  1));
        input = frequencies[li].note;
        one = frequencies[li].note;
    }
    else if (prms.game == 'Guess the interval') {

        li = Math.floor(l + Math.random() * (r - l +  1 - 12));
        ri = Math.floor(Math.random() * 12);
        one = frequencies[li].note;
        two = frequencies[li + ri].note;
        input = ri;
    }
    else if (prms.game == 'Build the interval') {

        li = Math.floor(l + Math.random() * (r - l +  1 - 12));
        ri = Math.floor(Math.random() * 12);
        one = frequencies[li].note;
        interval = intervals[ri];
        input = frequencies[li + ri].note;
    }

    repeat();

    if (prms.way == "Voice") {

        if (prms.range[0] == 'A0' || prms.range[0] == 'A0#' || prms.range[0] == 'B0') prms.range[0] = 'C1';
        if (prms.range[1] == 'C8' || prms.range[1] == 'B7' ||  prms.range[1] == 'A7#')  prms.range[1] = 'A7';

        neigbours = [   frequencies[li - 2],
                        frequencies[li - 1],
                        frequencies[li    ],
                        frequencies[li + 1],
                        frequencies[li + 2]    ];

        borders =   [   (frequencies[li - 3].pitch + frequencies[li - 2].pitch) / 2,
                        (frequencies[li - 2].pitch + frequencies[li - 1].pitch) / 2,
                        (frequencies[li - 1].pitch + frequencies[li    ].pitch) / 2,
                        (frequencies[li    ].pitch + frequencies[li + 1].pitch) / 2,
                        (frequencies[li + 1].pitch + frequencies[li + 2].pitch) / 2,  
                        (frequencies[li + 2].pitch + frequencies[li + 3].pitch) / 2];

        for (i = 0; i < 5; ++i)
            $('#voice-info-' + (i + 1)).hide().html('<label>' + neigbours[i].note + '</label>').fadeIn('slow');
        
    }
}

// Click event on repeat icon
$(document).on('click', '#repeat', function(e) {
    repeat();
});

// Repeat sound function
async function repeat() {
    $('#repeat').css("pointer-events", "none")
           .css({'transform': 'rotate(360deg)'});

    if (one) { 
        playPianoNote(one);         
        await sleep(2000);
    }
    if (two) { 
        playPianoNote(two);         
        await sleep(2000);
    }

    $('#repeat').css("pointer-events", "auto")
           .css({'transform': ''});
}

// Find note for game mode
function findNoteGameMode() {

    if (timer == 0) {
        console.log("stop");
        return;
    }

    // getting pitch
    let pitch = getFrequencyFromAudio();

    if (pitch == -1) 
        $('#voice-slider').css('opacity', '0.25');
    else {

            $('#voice-slider').css('opacity', '1')

            if (pitch < borders[0]) { 
                $('#voice-slider').css('left' , '0'); 
                $('#voice-info-1').css('background-color' ,'rgb(220, 20, 60, 0.2)'); 
                $('.voice-info').not('#voice-info-1').css('background-color' ,''); 
                voicecount = 0;
            }
            else if (pitch < borders[1]) {
                $('#voice-slider').css('left' , 0 + 20 * (pitch - borders[0]) / (borders[1] - borders[0]) + '%');
                $('#voice-info-1').css('background-color' ,'rgb(220, 20, 60, 0.2)'); 
                $('.voice-info').not('#voice-info-1').css('background-color' ,'');
                voicecount = 0;
            }
            else if (pitch < borders[2]) {
                $('#voice-slider').css('left' , 20 + 20 *(pitch - borders[1]) / (borders[2] - borders[1]) + '%');
                $('#voice-info-2').css('background-color', 'rgb(255, 215, 0, 0.2)'); 
                $('.voice-info').not('#voice-info-2').css('background-color' ,''); 
            }
            else if (pitch < borders[3]) {
                $('#voice-slider').css('left' , 40 + 20 *(pitch - borders[2]) / (borders[3] - borders[2]) + '%');
                $('#voice-info-3').css('background-color', 'rgb(34, 139, 34, 0.2)'); 
                $('.voice-info').not('#voice-info-3').css('background-color' ,'');
                voicecount++;
            }
            else if (pitch < borders[4]) {
                $('#voice-slider').css('left' , 60 + 20 *(pitch - borders[3]) / (borders[4] - borders[3]) + '%');
                $('#voice-info-4').css('background-color', 'rgb(255, 215, 0, 0.2)'); 
                $('.voice-info').not('#voice-info-4').css('background-color' ,''); 
            }
            else if (pitch < borders[5]) {
                $('#voice-slider').css('left' , 80 + 20 *(pitch - borders[4]) / (borders[5] - borders[4]) + '%');
                $('#voice-info-5').css('background-color' ,'rgb(220, 20, 60, 0.2)'); 
                $('.voice-info').not('#voice-info-5').css('background-color' ,''); 
                voicecount = 0;
            }
            else {
                // In most cases, high pitched sound represents defects (like noise).
                // To get rid of them, this block is commented.

                //$('#voice-slider').css('left' , '100%');
                //$('#voice-info-5').css('background-color' ,'rgb(220, 20, 60, 0.2)'); 
                //$('.voice-info').not('#voice-info-5').css('background-color' ,''); 
            }

            $('#progress-bar-filler').css('width', voicecount + '%');
            console.log(voicecount);
            if (voicecount == 100) {
                output = input;
                return;
            }
    }                                                

    setTimeout(requestAnimationFrame(function() {findNoteGameMode()}), 50);
}
