var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};


function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// ---------------------- Start menu animation functionality ---------------------- //

var flag_settings, flag_about;

function expandButton(obj) {
    $(obj).animate({height: "+=" + 400}, 500,
        function () { 
            $(obj).append('<img class="cross" src="img/cross.png"></img>');
            // TODO: .animate({opacity: 0}, 1000);
         });
}

function reduceButton(obj) {
    $(obj).animate({height: "-=" + 400}, 500);
    $(obj).empty();
}

// Expand settings button
$("#button-settings").click(function(e) {
    if (flag_settings) return;
    flag_settings = true;
    $("#button-about .cross").trigger("click");
    expandButton(this);
    $(this).append('<img class="cross" src="img/cross.png"></img>');
    return false;
});

// Expand about button
$("#button-about").click(function(e) {
    if (flag_about) return;
    flag_about = true;
    $("#button-settings .cross").trigger("click");
    expandButton(this);
    $(this).append('<img class="cross" src="img/cross.png"></img>');
});

// Reduce settings button
$(document).on('click', "#button-settings .cross", function(e) {
    if (!flag_settings) return;
    flag_settings = false;
    reduceButton($(this).parent());
    $("#button-settings").append('<label class="centring">Settings</label>');
});

// Reduce about button
$(document).on('click', "#button-about .cross", function(e) {
    if (!flag_about) return;
    flag_about = false;
    reduceButton($(this).parent());
    $("#button-about").append('<label class="centring">About</label>');
});

// ---------------------- Selection menu functionality ---------------------- //

// {game: guess-note|guess-interval|build-interval, way: piano|voice, range = [l, r],
//  count: 5..100, timer: 5...500 }
var prms = {};

// Expand start button
$("#button-start").click(function(e) {
    $("#button-settings .cross").trigger("click");
    $("#button-about .cross").trigger("click");
    showArea();

});

// Create area
function showArea() {
    $("#overlay").animate({opacity: 0.65}, 1000);
    $("#main-menu").animate({top: "0", opacity: 0}, 600,
        function(e) {
            $("#area").animate({opacity: 1}, 500,
            showGameSelection());
    });
}

function showGameSelection() {

    $("#content").append('<label class="title">SELECT THE GAME:</label>');

    let gameselmenu = $('<div/>', {
                id: 'game-sel-menu',
                class: 'sel-menu', 
    }); 

    for (var i = 0; i < 3; ++i) {
        let button = $('<label/>', {
                    id: 'game-sel-menu-item-' + (i + 1),
                    class: 'game-sel-menu-item', 
                    click: function() { gameSelMenuSelected(this) }
        }); 

        $(gameselmenu).append(button);
    } 

    $("#content").append(gameselmenu);

    $('#game-sel-menu-item-1').html('Guess the note');
    $('#game-sel-menu-item-2').html('Guess the interval');
    $('#game-sel-menu-item-3').html('Build the interval');

    $("#content").append('<label class="title">SELECT THE WAY TO ANSWER:</label>');

    let toolselmenu = $('<div/>', {
                id: 'tool-sel-menu',
                class: 'sel-menu', 
    }); 

    for (var i = 0; i < 2; ++i) {
        let button = $('<label/>', {
                    id: 'tool-sel-menu-item-' + (i + 1),
                    class: 'tool-sel-menu-item', 
                    click: function() { toolSelMenuSelected(this) }
        }); 

        $(toolselmenu).append(button);
    } 

    $("#content").append(toolselmenu);

    $('#tool-sel-menu-item-1').html('Piano');
    $('#tool-sel-menu-item-2').html('Voice');

    $("#content").append('<label class="title">CHECK MUSICAL RANGE:</label>');

    createRangeButton();
    wrapper = createPiano('range');
    wrapper.css("top", "250px");
    $('#content').append(wrapper);
    $('#piano').scrollLeft(($('#piano-white').width() - $('#piano').width()) / 2);

    createAdditionalParams(); 

    createStartButton();
}

function createRangeButton() {
    let button = $('<label/>', { id: 'btn-check-range', 
                                class: 'centering-x',
                                text: "What\'s my vocal range?" });
    $('#content').append(button);
}

function createAdditionalParams() {
    let addselmenu = $('<div/>', {
                id: 'add-sel-menu',
                class: 'sel-menu centering-x', 
    }); 

    let count = $('<div/>', { id: 'count-sel-menu', class: 'add-sel-menu-item' }); 
    let timer = $('<div/>', { id: 'timer-sel-menu', class: 'add-sel-menu-item' }); 

    count.append('<label>Count (5..100): </label><input type="text" id="count-sel-menu-input" value="10">');
    timer.append('<label>Timer (15..500): </label><input type="text" id="timer-sel-menu-input" value="20">');

    $(addselmenu).append(count);
    $(addselmenu).append(timer);

    $("#content").append(addselmenu);
}

$(document).on('click', '#count-sel-menu-input', function(e) { 
    $('#count-sel-menu-input').css('background-color', '');
});

$(document).on('click', '#timer-sel-menu-input', function(e) { 
    $('#timer-sel-menu-input').css('background-color', '');
});

function createStartButton() {

    let start = $('<div/>', { id: 'btn-start',
                              class: 'title centering-x btn-start-hover', 
                              click: function() {
                              //alert($(this).attr('data-system-id'));
                            }
    }); 

    start.html('<label>START THE GAME</label>');

    $('#content').append(start);    
}

// ---------------------- Selection buttons behavior ---------------------- //

function gameSelMenuSelected(obj) {
        if ($(obj).hasClass('menu-item-selected'))
        {
            $(obj).removeClass('menu-item-selected').removeAttr("style");
            delete prms.game;
        }
        else {
            $('.game-sel-menu-item').removeAttr("style");
            $('.game-sel-menu-item').not(obj).removeClass('menu-item-selected');
            $(obj).addClass('menu-item-selected');
            prms.game = $(obj).text();

            if (prms.game == "Guess the interval") {
                $('.tool-sel-menu-item').removeClass('menu-item-selected');
                $('.tool-sel-menu-item').animate({opacity: "0.25"}, 250);
                $('.tool-sel-menu-item').css("pointer-events","none");
                prms.way = "None";
            }
            else {
                $('.tool-sel-menu-item').css("pointer-events","auto");
                $('.tool-sel-menu-item').animate({opacity: "1"}, 250);
                if (prms.way == "None") 
                    delete prms.way;

            }
        }
    }

function toolSelMenuSelected(obj) {
        if ($(obj).hasClass('menu-item-selected'))
        {
            $(obj).removeClass('menu-item-selected').removeAttr("style");
            delete prms.way;
        }
        else {
            $('.tool-sel-menu-item').removeAttr("style");
            $('.tool-sel-menu-item').not(obj).removeClass('menu-item-selected');
            $(obj).addClass('menu-item-selected');
            prms.way = $(obj).text();
        }
    }

// ---------------------- Piano functionality ---------------------- //

function createPiano(string) {

    let wrapper = $('<div/>', { id: 'piano-wrapper' });
    let piano = $('<div/>', { id: 'piano', class: 'piano' }); 

    let piano_white = $('<div/>', { id: 'piano-white' }); 
    let piano_black = $('<div/>', { id: 'piano-black' }); 

    let letters = ['C', 'D', 'E', 'F', 'G', 'A', 'B'];

    // -------- Add keys --------
    piano_white.append('<div id="A0" class="piano-key-white ' + string +'""></div>');
    piano_white.append('<div id="B0" class="piano-key-white ' + string +'""></div>');

    for (i = 1; i <= 7; ++i)
        for (j = 0; j < 7; ++j)
            piano_white.append('<div id="' + letters[j] + i + '" class="piano-key-white ' + string +'"></div');
    
    piano_white.append('<div id="C8" class="piano-key-white ' + string +'"></div>');

    // -------- Add black keys --------
    piano_black.append('<div id="A0#" class="piano-key-black ' + string +'" style="margin-right: 7.5px; border-width: 1.5px"></div>');   

    for (i = 1; i <= 7; ++i)
    {
        let key;
        for (j = 0; j < 7; ++j)
        {
            key = $('<div/>', { id: letters[j] + i + '#',
                                class: 'piano-key-black ' + string}); 

            if (letters[j] == 'A' || letters[j] == 'D') {
                key.css({"margin-right": "7.5px",
                         "border-right-width": "1.5px"});
            }
            else if (letters[j] == 'B' || letters[j] == 'E') {
                continue;
            } 
            else if (letters[j] == 'C' || letters[j] == 'F') {
                key.css({"margin-left": "7.5px",
                         "border-left-width": "1.5px"});
            }
            
            piano_black.append(key);
        }
    }  

    piano.append(piano_white);
    piano.append(piano_black);

    let info = $('<div/>', { id: 'piano-info' }); 
    info.append($('<div/>', { class: 'piano-octaves', text: '(0)' , style: "border-left-width: 0; width: 31.5px;"}));
    info.append($('<div/>', { class: 'piano-octaves', text: 'Contra (1)'}));
    info.append($('<div/>', { class: 'piano-octaves', text: 'Great (2)'}));
    info.append($('<div/>', { class: 'piano-octaves', text: 'Small (3)'}));
    info.append($('<div/>', { class: 'piano-octaves', text: 'First (4)'}));
    info.append($('<div/>', { class: 'piano-octaves', text: 'Second (5)'}));
    info.append($('<div/>', { class: 'piano-octaves', text: 'Third (6)'}));
    info.append($('<div/>', { class: 'piano-octaves', text: 'Fourth (7)'}));
    info.append($('<div/>', { class: 'piano-octaves', style: "border-right-width: 0; width: 15px;"}));

    piano.append(info);

    $(wrapper).append(piano);
    return wrapper;
    }

prms.range = [];

$(document).on('click', '.range', function(e) { 
    $('#piano-wrapper').css("background-color", "rgb(0,0,0,0)");
    selectPianoKey(this) 
} );

function selectPianoKey(key) {
    console.log(prms.range.length + " " + prms.range[0]);
    if (prms.range.length == 0 || prms.range[0] == undefined) {
        prms.range.length = [];
        prms.range.push($(key).attr('id'));
        prms.range.push($(key).attr('id'));
        selectRange();
    }
    else {
        console.log(prms.range.length);
        let len = -compareNotes(prms.range[0], prms.range[1]);

        let ldist = compareNotes($(key).attr('id'), prms.range[0]);
        if (ldist < 0 || ldist <= (len / 2)) {

            prms.range[0] = $(key).attr('id');
            selectRange();
            return;
        }

        let rdist = compareNotes($(key).attr('id'), prms.range[1]);
        if (rdist > 0 || -rdist <= (len / 2)) {

            prms.range[1] = $(key).attr('id');
            selectRange();
            return;
        }
    }
}

function Note(string) {
    
    this.str = string;
    this.octave = string[1];
    this.letter = (string.length == 2) ? string[0] : string[0] + string[2];

    this.getPrevNote = function() {
        
        return (this.letter == 'C') ? new Note( craftNote(this.octave - 1, 'B') ) : 
                                      new Note( craftNote(this.octave, notes[notes.indexOf(this.letter) - 1]) );
    }

    this.getNextNote = function() {

        return (this.letter == 'B') ? new Note( craftNote(this.octave - 1, 'C') ) : 
                                      new Note( craftNote(this.octave, notes[notes.indexOf(this.letter) + 1]) );
    }
}

// ---------------------- Piano Range functionality ---------------------- //

letters = {'C' : 0, 'D' : 1, 'E' : 2, 'F' : 3, 'G' : 4, 'A' : 5, 'B' : 6};

function compareNotes(l, r) {
    let a = letters[l.charAt(0)] + parseInt(l.charAt(1)) * 10 + (l.charAt(2) == '#' ? 0.5 : 0);
    let b = letters[r.charAt(0)] + parseInt(r.charAt(1)) * 10 + (r.charAt(2) == '#' ? 0.5 : 0);

    return a - b;
}

function selectRange() {

    $('.range').each(function(e) {

        let l = compareNotes($(this).attr('id'), prms.range[0]);
        let r = compareNotes($(this).attr('id'), prms.range[1]);

        if (l >= 0 && r <= 0)
        {
            if ($(this).hasClass('piano-key-white'))
                $(this).css("background-color", "rgba(255, 255, 255, 0.8)");
            else
                $(this).css("background-color", "#202020");
        }
        else
            $(this).css("background-color", "");
        
    });
}

// ---------------------- Find Range Mode functionality ---------------------- //

$(document).on('click', '#btn-check-range', function(e) {

    $('#piano-wrapper').css("background-color", "rgb(0,0,0,0)");

    let info = $('<label/>', { id: 'info', 
                                class: 'centering-x hidden',
                                text: "Please, allow to use your microphone.." });
    $('#content').append(info);

    $('#content').children().not('#piano-wrapper').animate({opacity: 0}, 250, function(e) {
        $('#content').children().not('#piano-wrapper').not('.cross').hide();
    });

    $('#piano-wrapper').animate({top: '50'}, 800, function(e) { 
        $('#info').show().animate({opacity: 1}, 500)
    });

    $('#content').append('<img class="cross" src="img/cross.png"></img>');

    getAudioRangeMode();
});

$(document).on('click', '#content .cross', function(e) {

    stopAudio();

    $('.cross').remove();

    $('#info').animate({opacity: 0}, 300, function(e) {
        $('#content').children().not('#piano-wrapper').show();
        $('#info').remove();
    })

    $('#piano-wrapper').animate({top: '260'}, 800, function(e) { 
        $('#content').children().not('#piano-wrapper').animate({opacity: 1}, 250, function(e) {
        });
    });
});

function changeTextAnimation(obj, text) {
    $(obj).animate({top: '-=' + 20, opacity: 0}, 250, function(e) {
        $(obj).html(text);
        $(obj).css('top', $(obj).position().top + 40);
        $(obj).animate({top: '300px', opacity: 1}, 250);
    });
}

$(document).on('click', '#info-try-again', function(e) {
    getAudioRangeMode();
    changeTextAnimation('#info', 'Please, allow to use your microphone..');
});

// Helpful array for note clarity detection
var arrayDetection = [];

function getAudioRangeMode() {
   navigator.getUserMedia({audio: true, video: false}, 
        function (stream) {
            getAudio(stream);

            if (!$('#btn-start').is(":hidden")) {
                stopAudio(); 
                return;
            }; 

            prms.range = [];
            flagsRangeMode = [true, true];

            $('.range').each(function(e) { 
                $(this).css("background-color", "") });

            detectRangeMode();
    }, function (error) { errorRangeMode('info') })
}

var flagsRangeMode = [true, true]

async function detectRangeMode() {
    if (flagsRangeMode[0]) {
            console.log("h");

        arrayDetection = [];
        changeTextAnimation('#info', 'Find the <b>lowest</b> note you can sing in your normal voice. \
                                            Try to sing it clearly...');
        flagsRangeMode[0] = false;
    }

    if (!prms.range[0]) {
        
        prms.range[0] = findNoteRangeMode();
        return;
    }

    if (flagsRangeMode[1]) {
        arrayDetection = [];
        changeTextAnimation('#info', 'Great!');
        await sleep(2000);
        changeTextAnimation('#info', 'Find the <b>highest</b> note you can sing in your normal voice. \
                                            Try to sing it clearly...');
        flagsRangeMode[1] = false;
    }

    if (!prms.range[1]) {
        prms.range[1] = findNoteRangeMode();
        return;
    }

    changeTextAnimation('#info', 'Splendid!');
    await sleep(2000);
    selectPianoKey('[id="' + prms.range[0] + '"]');
    selectPianoKey('[id="' + prms.range[1] + '"]');

    $('#content .cross').trigger("click");
}

function errorRangeMode(id) {
    changeTextAnimation('#' + id +'', 'Microphone is not detected. \
                        <div id="' + id + '-try-again">Click here to try again</div>');
}

function findNoteRangeMode(stop) {

    // getting note string
    let str = getNoteFromAudio();
    let note;

    if (str != -1 && str.length < 4) {
                                                    
        // example: ['C2' : {note: Note('C2'), count: 1}]

        curr = new Note(str);
        ccurr = (arrayDetection[curr.str]) ? arrayDetection[curr.str].count : 0;
                  
        prev = curr.getPrevNote();
        cprev = (arrayDetection[prev.str]) ? arrayDetection[prev.str].count : 0;

        next = curr.getNextNote();
        cnext = (arrayDetection[next.str]) ? arrayDetection[next.str].count : 0;

        // clear detection array
        // arrayDetection = []

        arrayDetection[curr.str] = { note: curr, count: ccurr + 1 };
        arrayDetection[prev.str] = { note: prev, count: cprev + 0.5 };
        arrayDetection[next.str] = { note: next, count: cnext + 0.5 };

        if (   arrayDetection[curr.str].count >= 100
            && arrayDetection[curr.str].count <= 102) {            
            detectRangeMode();
            return arrayDetection[curr.str].note.str;
        }

        console.log(arrayDetection);
    }

    setTimeout(requestAnimationFrame(function() {detectRangeMode()}), 50);
}

$(document).on('click', '#btn-start', function(e) {
    gameSelStart(this)
});

function gameSelStart(obj) {

    let timer = parseInt($('#timer-sel-menu-input').val());
    let count = parseInt($('#count-sel-menu-input').val());

    if (prms.range.length != 2 || prms.range[0] == undefined || prms.range[1] == undefined ||
        prms.game == undefined || prms.way == undefined || 
        isNaN(count) || count < 5 || count > 100 || isNaN(timer) || timer < 15 || timer > 500) {

        if (prms.range.length != 2 || prms.range[0] == undefined || prms.range[1] == undefined)
            $("#piano-wrapper").css('background-color', 'rgb(255, 150, 160, 0.3)');

        if (prms.game == undefined)
            $(".game-sel-menu-item").css('background-color', 'rgb(255, 150, 160, 0.3)');

        if (prms.way == undefined)
            $(".tool-sel-menu-item").css('background-color', 'rgb(255, 150, 160, 0.3)'); 

        if (isNaN(count) || count < 5 || count > 100)       
            $('#count-sel-menu-input').css('background-color', 'rgb(255, 150, 160, 0.3)');

        if (isNaN(timer) || timer < 15 || timer > 500)       
            $('#timer-sel-menu-input').css('background-color', 'rgb(255, 150, 160, 0.3)');

        return;
    }

    prms.timer = timer;
    prms.count = count;

    let info = $('<label/>', { id: 'info', 
                            class: 'centering-x hidden',
                            text: "Please, allow to use your microphone.." });
    $('#content').append(info);

    flag = true;
    $('#content').children().animate({opacity: 0}, 250, function(e) {
        if (flag) {
            $('#content').children().remove();
            buildGameArea();
            flag = false;
        }
    });
}

var timer;
var count = {}; // total, win, loose
var neigbours = [];
var borders = [];
var l, r;
var voicecount;

function buildGameArea() {

    if (prms.way == "Piano") {

        //question = createNoteBox(); // Animation of note
        answer = createPiano("way");
        
    }
    else if (prms.way == "Voice") {

        question = createPiano("game");
        answer = createVoiceInd();
    }
    else {
        question = createPiano("game");
        answer = createIntervalBox();
    }

    $('#content').append(question);
    question.addClass("top hidden");
    $("#piano-wrapper").animate({opacity: 1}, 250);


    $('#content').append(answer);
    answer.addClass("bottom hidden");
    $("#voice-wrapper").animate({opacity: 1}, 250);



    let info = $('<label/>', { id: 'info', 
                                class: 'centering-x hidden',
                                text: "Please, allow to use your microphone.." });
    $('#content').append(info);
    $("#info").animate({opacity: 1}, 250);

    let params = $('<div/>', {
                id: 'game-params',
                class: 'game-params centering-x', 
    }); 

    let timer = $('<div/>', { id: 'timer-game-params', class: 'game-params-item' }); 
    let count = $('<div/>', { id: 'count-game-params', class: 'game-params-item' }); 

    timer.append('<label id="game-left">Time left: </label>');
    count.append('<label id="game-win">Win: </label> \
                  <label id="game-loose">Loose: </label>\
                  <label id="game-total">Total: </label>');

    $(params).append(timer);
    $(params).append(count);

    $("#content").append(params);

    if (prms.way == 'Voice') 
        getAudioGamePlay();
    else {
        timer = prms.timer;
        count = {left: prms.count, win: 0, loose: 0};
        tickGame();
    }
}

function createNotebox() {

}

function createIntervalBox() {
    let wrapper = $('<div/>', { id: 'interval-wrapper', 
                                class: 'centering-x hidden' });

    wrapper.animate({opacity: 1}, 500)    

    for (i = 0; i < 12; ++i) {
        interval = intervals[i];
        block = $('<label/>', { id: 'interval-' + i, 
                                class: 'interval hidden',
                                text: intervals[i] });
        wrapper.append(block);
    }

    return wrapper;
}

function createVoiceInd() {

    let wrapper = $('<div/>', { id: 'voice-wrapper', 
                                class: 'centering-x hidden' });

    wrapper.animate({opacity: 1}, 500)


    let ind = $('<div/>', { id: 'voice-wrapper-ind' });
    let block = $('<div/>', { id: 'voice-wrapper-block' });
    let info = $('<div/>', { id: 'voice-wrapper-info' });

    for (i = 0; i < 5; ++i) {
        block.append("<div id='voice-block-" + (i + 1) + "' class='voice-block'></div>");
    }

    for (i = 0; i < 5; ++i) {
        info.append("<div id='voice-info-" + (i + 1) + "' class='voice-info'></div>");
    }

    let slider = $('<div/>', { id: 'voice-slider', class: 'hidden'});

    slider.append('<div id="voice-slider-circle"></div>');
    slider.append('<div id="voice-slider-triangle"></div>');

    ind.append(slider);

    block.children().eq(0).css({'border-left-width' : '0',
                             'border-top-left-radius' : '5px',
                             'background-color' : 'crimson' });
    block.children().eq(1).css( 'background-color', 'gold');
    block.children().eq(2).css( 'background-color', 'forestgreen');
    block.children().eq(3).css( 'background-color', 'gold');
    block.children().eq(4).css({'background-color': 'crimson',
                             'border-right-width' : '0',
                             'border-top-right-radius' : '5px' });

    info.children().eq(0).css({'border-left-width' : '0',
                            'border-bottom-left-radius' : '5px',
                            'background-color' :'rgb(220, 20, 60, 0)' });
    info.children().eq(1).css( 'background-color', 'rgb(255, 215, 0, 0)');
    info.children().eq(2).css( 'background-color', 'rgb(34, 139, 34, 0)');
    info.children().eq(3).css( 'background-color', 'rgb(255, 215, 0, 0)');
    info.children().eq(4).css({'background-color': 'rgb(220, 20, 60, 0)',
                            'border-right-width' : '0',
                            'border-bottom-right-radius' : '5px' });

    wrapper.append(ind);
    wrapper.append(block);
    wrapper.append(info);

    return wrapper;
}

function getAudioGamePlay() {
    navigator.getUserMedia({audio: true, video: false}, 
        function (stream) {
            getAudio(stream);

            if ($('#info').is(":hidden")) {
                stopAudio(); 
               return;
            }; 

            timer = prms.timer;
            count = {left: prms.count, win: 0, loose: 0};
            tickGame();

    }, function (error) { errorRangeMode('info') })
}

var output;

async function tickGame() {

    if (output == undefined) {

        if (timer == prms.timer) {

            if (count.left == prms.count) changeTextAnimation('#info', 'Are you ready?');
            await sleep(2000);

            // Генерируем новый input
            input = getInput();
            
            // TODO: INDIVIDUAL FOR EVERY GAME AND WAY
            changeTextAnimation('#info', "Note:\t" + input.note);

            voicecount = 0;
            findNoteGameMode(stop);
        }
        if (timer == 0) {

            changeTextAnimation('#info', "Ooops, time is left!");

            timer = prms.timer;
            count.loose++;
            count.left--;
        }
        else 
            timer--; 

        $('#game-left').text('Time left: ' + timer);
    }
    else {

        timer = prms.timer;
        count.left--;

        if (input == output) {

            count.win++;
            changeTextAnimation('#info', "You've got it!");
        } 
        else { 
            count.loose++;
            changeTextAnimation('#info', "No, you are wrong :(");
        }

        output = undefined;
    }

    $('#game-win').text('Win: ' + count.win);    
    $('#game-loose').text("Loose: " + count.loose);
    $('#game-total').text('Total attempts: ' + (prms.count - count.left));

    if (count.left != 0)
        setTimeout(function() { tickGame(); }, 1000);
    else {
        changeTextAnimation('#info', "No, you are wrong :(");
    }
}

function getInput() {
 
    l = frequencies.findIndex(function(element, index, array)  {
        if (prms.range[0] == "A0") return 1;
        if (element.note == prms.range[0]) return index;
    });

    r = frequencies.findIndex(function(element, index, array)  {
        if (element.note == prms.range[1]) return index;
    });

    let input, li, ri;

    if (prms.game == 'Guess the note') {
        
        li = Math.floor(l + Math.random() * (r - l +  1));
        input = frequencies[li];
        playNote(input.pitch);

    }
    else if (prms.game == 'Guess the interval') {

        li = Math.floor(l + Math.random() * (r - l +  1 - 12));
        ri = Math.floor(1 + Math.random() * 13);
        note = frequencies[li];
        playNote(note.pitch);
        playNote(frequencies[li + ri].pitch);
        input = intervals[ri];
    }
    else if (prms.game == 'Build the interval') {

        li = Math.floor(l + Math.random() * (r - l +  1 - 12));
        ri = Math.floor(1 + Math.random() * 13);
        note = frequencies[li];
        playNote(note.pitch);
        interval = intervals[ri];
        input = frequencies[li + ri];
    }

    if (prms.way == "Voice") {

        if (prms.range[0] == 'A0' || prms.range[0] == 'A0#' || prms.range[0] == 'B0') prms.range[0] = 'C1';
        if (prms.range[1] == 'C8' || prms.range[1] == 'B7' ||  prms.range[1] == 'A7#')  prms.range[1] = 'A7';

        neigbours = [   frequencies[li - 2],
                        frequencies[li - 1],
                        frequencies[li    ],
                        frequencies[li + 1],
                        frequencies[li + 2]    ];

        borders =   [   (frequencies[li - 3].pitch + frequencies[li - 2].pitch) / 2,
                        (frequencies[li - 2].pitch + frequencies[li - 1].pitch) / 2,
                        (frequencies[li - 1].pitch + frequencies[li    ].pitch) / 2,
                        (frequencies[li    ].pitch + frequencies[li + 1].pitch) / 2,
                        (frequencies[li + 1].pitch + frequencies[li + 2].pitch) / 2,  
                        (frequencies[li + 2].pitch + frequencies[li + 3].pitch) / 2];

        for (i = 0; i < 5; ++i)
            $('#voice-info-' + (i + 1)).hide().html('<label>' + neigbours[i].note + '</label>').fadeIn('slow');
        
    }

    return input;
}

function findNoteGameMode() {

    if (timer == 0) {
        console.log("stop");
        return;
    }

    // getting pitch
    let pitch = getFrequencyFromAudio();

    if (pitch == -1) 
        $('#voice-slider').css('opacity', '0.25');
    else {

            $('#voice-slider').css('opacity', '1')

            if (pitch < borders[0]) { 
                $('#voice-slider').css('left' , '0'); 
                $('#voice-info-1').css('background-color' ,'rgb(220, 20, 60, 0.2)'); 
                $('.voice-info').not('#voice-info-1').css('background-color' ,''); 
                voicecount = 0;
            }
            else if (pitch < borders[1]) {
                $('#voice-slider').css('left' , 0 + 20 * (pitch - borders[0]) / (borders[1] - borders[0]) + '%');
                $('#voice-info-1').css('background-color' ,'rgb(220, 20, 60, 0.2)'); 
                $('.voice-info').not('#voice-info-1').css('background-color' ,'');
                voicecount = 0;
            }
            else if (pitch < borders[2]) {
                $('#voice-slider').css('left' , 20 + 20 *(pitch - borders[1]) / (borders[2] - borders[1]) + '%');
                $('#voice-info-2').css('background-color', 'rgb(255, 215, 0, 0.2)'); 
                $('.voice-info').not('#voice-info-2').css('background-color' ,''); 
            }
            else if (pitch < borders[3]) {
                $('#voice-slider').css('left' , 40 + 20 *(pitch - borders[2]) / (borders[3] - borders[2]) + '%');
                $('#voice-info-3').css('background-color', 'rgb(34, 139, 34, 0.2)'); 
                $('.voice-info').not('#voice-info-3').css('background-color' ,'');
                voicecount++;
            }
            else if (pitch < borders[4]) {
                $('#voice-slider').css('left' , 60 + 20 *(pitch - borders[3]) / (borders[4] - borders[3]) + '%');
                $('#voice-info-4').css('background-color', 'rgb(255, 215, 0, 0.2)'); 
                $('.voice-info').not('#voice-info-4').css('background-color' ,''); 
            }
            else if (pitch < borders[5]) {
                $('#voice-slider').css('left' , 80 + 20 *(pitch - borders[4]) / (borders[5] - borders[4]) + '%');
                $('#voice-info-5').css('background-color' ,'rgb(220, 20, 60, 0.2)'); 
                $('.voice-info').not('#voice-info-5').css('background-color' ,''); 
                voicecount = 0;
            }
            else if (pitch > borders[5]) {

                // In most cases, high pitched sound represents defects (like noise).
                // To get rid of them, this block is commented.

                //$('#voice-slider').css('left' , '100%');
                //$('#voice-info-5').css('background-color' ,'rgb(220, 20, 60, 0.2)'); 
                //$('.voice-info').not('#voice-info-5').css('background-color' ,''); 
            } 
                
        if (voicecount == 100) {
            output = input;
            return;
        }
    }                                                

    setTimeout(requestAnimationFrame(function() {findNoteGameMode()}), 50);

}