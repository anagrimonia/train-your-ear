// ---------------------- Piano functionality ---------------------- //

// Create piano object
function createPiano(string) {

    let wrapper = $('<div/>', { id: 'piano-wrapper' });
    let piano = $('<div/>', { id: 'piano', class: 'piano' }); 

    let piano_white = $('<div/>', { id: 'piano-white' }); 
    let piano_black = $('<div/>', { id: 'piano-black' }); 

    let letters = ['C', 'D', 'E', 'F', 'G', 'A', 'B'];

    // -------- Add keys --------
    piano_white.append('<div id="A0" class="piano-key-white ' + string +'""></div>');
    piano_white.append('<div id="B0" class="piano-key-white ' + string +'""></div>');

    for (i = 1; i <= 7; ++i)
        for (j = 0; j < 7; ++j)
            piano_white.append('<div id="' + letters[j] + i + '" class="piano-key-white ' + string +'"></div');
    
    piano_white.append('<div id="C8" class="piano-key-white ' + string +'"></div>');

    // -------- Add black keys --------
    piano_black.append('<div id="A0#" class="piano-key-black ' + string +'" style="margin-right: 7.5px; border-width: 1.5px"></div>');   

    for (i = 1; i <= 7; ++i)
    {
        let key;
        for (j = 0; j < 7; ++j)
        {
            key = $('<div/>', { id: letters[j] + i + '#',
                                class: 'piano-key-black ' + string}); 

            if (letters[j] == 'A' || letters[j] == 'D') {
                key.css({"margin-right": "7.5px",
                         "border-right-width": "1.5px"});
            }
            else if (letters[j] == 'B' || letters[j] == 'E') {
                continue;
            } 
            else if (letters[j] == 'C' || letters[j] == 'F') {
                key.css({"margin-left": "7.5px",
                         "border-left-width": "1.5px"});
            }
            
            piano_black.append(key);
        }
    }  

    piano.append(piano_white);
    piano.append(piano_black);

    let info = $('<div/>', { id: 'piano-info' }); 
    info.append($('<div/>', { class: 'piano-octaves', text: '(0)' , style: "border-left-width: 0; width: 31.5px;"}));
    info.append($('<div/>', { class: 'piano-octaves', text: 'Contra (1)'}));
    info.append($('<div/>', { class: 'piano-octaves', text: 'Great (2)'}));
    info.append($('<div/>', { class: 'piano-octaves', text: 'Small (3)'}));
    info.append($('<div/>', { class: 'piano-octaves', text: 'First (4)'}));
    info.append($('<div/>', { class: 'piano-octaves', text: 'Second (5)'}));
    info.append($('<div/>', { class: 'piano-octaves', text: 'Third (6)'}));
    info.append($('<div/>', { class: 'piano-octaves', text: 'Fourth (7)'}));
    info.append($('<div/>', { class: 'piano-octaves', style: "border-right-width: 0; width: 15px;"}));

    piano.append(info);

    $(wrapper).append(piano);
    return wrapper;
    }

// Range parameter for the game
prms.range = [];

// Click event on piano keys for range selection
$(document).on('click', '.range', function(e) { 
    $('#piano-wrapper').css("background-color", "rgb(0,0,0,0)");
    selectPianoKey(this) 
} );

// Select piano key
function selectPianoKey(key) {
    console.log(prms.range.length + " " + prms.range[0]);
    if (prms.range.length == 0 || prms.range[0] == undefined || prms.range[1] == undefined ) {
        prms.range.length = [];
        prms.range.push($(key).attr('id'));
        prms.range.push($(key).attr('id'));
        selectRange();
    }
    else {
        console.log(prms.range.length);
        let len = -compareNotes(prms.range[0], prms.range[1]);

        let ldist = compareNotes($(key).attr('id'), prms.range[0]);
        if (ldist < 0 || ldist <= (len / 2)) {

            prms.range[0] = $(key).attr('id');
            selectRange();
            return;
        }

        let rdist = compareNotes($(key).attr('id'), prms.range[1]);
        if (rdist > 0 || -rdist <= (len / 2)) {

            prms.range[1] = $(key).attr('id');
            selectRange();
            return;
        }
    }
}

// Create note box object
function createNotebox() {
    let wrapper = $('<div/>', { id: 'box-wrapper', 
                                class: 'centering-x' });

    return wrapper;
}

// Create interval box object
function createIntervalBox() {
    let wrapper = $('<div/>', { id: 'interval-wrapper', 
                                class: 'centering-x' });

    for (i = 0; i < 12; ++i) {
        interval = intervals[i];
        block = $('<label/>', { id: i + '-interval', 
                                class: 'interval',
                                text: intervals[i] });
        wrapper.append(block);
    }

    return wrapper;
}

// Click event on interval list buttons 
$(document).on('click', '.interval', function(e) {
    $(this).addClass('interval-selected');
    $('.interval').not(this).removeClass('interval-selected');
    output = $(this).attr('id').split('-')[0];
});

// Click event on piano key if piano is a supporting object
$(document).on('click', '.piano-game', async function(e) {
    //$('.piano-game').css("pointer-events", "none");
    tmp = $(this).attr('id');
    playPianoNote(tmp);
    //await sleep(1000);
    //$('.piano-game').css("pointer-events", "");
});

// Click event on piano key for game modes
$(document).on('click', '.piano-way', async function(e) {
    if (timer < prms.timer - 1 && timer != 0) {
        $('.piano-way').css("pointer-events", "none");
        tmp = $(this).attr('id');
        output = tmp;
        playPianoNote(tmp);
        await sleep(1000);
        $('.piano-way').css("pointer-events", "");
    }
});

// Create voice indicator object
function createVoiceInd() {

    let wrapper = $('<div/>', { id: 'voice-wrapper', 
                                class: 'centering-x hidden' });

    wrapper.animate({opacity: 1}, 500)

    wrapper.append('<div id="progress-bar" style="top: -20px" class="centering-x"><div id="progress-bar-filler"></div></div>');

    let ind = $('<div/>', { id: 'voice-wrapper-ind' });
    let block = $('<div/>', { id: 'voice-wrapper-block' });
    let info = $('<div/>', { id: 'voice-wrapper-info' });

    for (i = 0; i < 5; ++i) {
        block.append("<div id='voice-block-" + (i + 1) + "' class='voice-block'></div>");
    }

    for (i = 0; i < 5; ++i) {
        info.append("<div id='voice-info-" + (i + 1) + "' class='voice-info'></div>");
    }

    let slider = $('<div/>', { id: 'voice-slider', class: 'hidden'});

    slider.append('<div id="voice-slider-circle"></div>');
    slider.append('<div id="voice-slider-triangle"></div>');

    ind.append(slider);

    block.children().eq(0).css({'border-left-width' : '0',
                             'border-top-left-radius' : '5px',
                             'background-color' : 'crimson' });
    block.children().eq(1).css( 'background-color', 'gold');
    block.children().eq(2).css( 'background-color', 'forestgreen');
    block.children().eq(3).css( 'background-color', 'gold');
    block.children().eq(4).css({'background-color': 'crimson',
                             'border-right-width' : '0',
                             'border-top-right-radius' : '5px' });

    info.children().eq(0).css({'border-left-width' : '0',
                            'border-bottom-left-radius' : '5px',
                            'background-color' :'rgb(220, 20, 60, 0)' });
    info.children().eq(1).css( 'background-color', 'rgb(255, 215, 0, 0)');
    info.children().eq(2).css( 'background-color', 'rgb(34, 139, 34, 0)');
    info.children().eq(3).css( 'background-color', 'rgb(255, 215, 0, 0)');
    info.children().eq(4).css({'background-color': 'rgb(220, 20, 60, 0)',
                            'border-right-width' : '0',
                            'border-bottom-right-radius' : '5px' });

    wrapper.append(ind);
    wrapper.append(block);
    wrapper.append(info);

    return wrapper;
}